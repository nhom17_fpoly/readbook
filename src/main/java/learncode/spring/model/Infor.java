package learncode.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Information")
public class Infor {
	@Id
	@Column(length = 50)
	private String name;
	
	@Column(length = 50)
	private  boolean gender;
	
	@Column
	private int phone;
	
	@Column(length = 50)
	private String adress;

	public Infor() {
		super();
	}

	public Infor(String name, boolean gender, int phone, String adress) {
		super();
		this.name = name;
		this.gender = gender;
		this.phone = phone;
		this.adress = adress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}
	
}
