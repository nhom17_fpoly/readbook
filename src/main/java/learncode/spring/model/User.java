package learncode.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User{
	@Id
	@Column(name="email",length = 50)
	private String email;
	
	@Column(name="password",length = 50)
	private String password;
	
	@Column(name="fullname",length = 50)
	private String fullname;

	public User() {
		super();
	}

	public User(String email, String password, String fullname) {
		super();
		this.email = email;
		this.password = password;
		this.fullname = fullname;
	}
	
	
}
