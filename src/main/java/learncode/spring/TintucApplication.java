package learncode.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TintucApplication {

	public static void main(String[] args) {
		SpringApplication.run(TintucApplication.class, args);
	}

}
